var gulp        = require('gulp');
var pump        = require('pump');
var sass        = require('gulp-sass');
var gutil       = require('gulp-util');
var watch       = require('gulp-watch');
var uglify      = require('gulp-uglify');
var sourcemaps  = require('gulp-sourcemaps');

var path = 'src/module/resources/';

gulp.task('styles', function() {
    return gulp.src(path+'src/css/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(path+'public/css'));
});

gulp.task('scripts', function(cb) {
    pump([
            gulp.src(path+'src/js/**/*.js'),
            ourcemaps.init(),
            uglify().on('error', gutil.log),
            sourcemaps.write('./'),
            gulp.dest(path+'public/js')
        ],
        cb
    );
});

gulp.task('watch', function() {
    gulp.watch(path+'src/css/**/*.scss', ['styles']);
    gulp.watch(path+'src/js/**/*.js', ['scripts']);
});

gulp.task('default', ['watch']);