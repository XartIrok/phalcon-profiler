<?php
namespace Phalcon\Profiler\Controllers;

use Phalcon\Mvc\Controller;

class TokenController extends Controller
{
    protected $pathLog;

    public function initialize()
    {
        $viewDir = __DIR__ . '/../resources/views/';
        $this->view->setViewsDir($viewDir);
        $this->view->setMainView("profiler");
        $this->view->render("token", "index");

        $this->pathLog = DEBUG_PATH_LOG;
    }

    public function indexAction($token = null, $panel = 'request')
    {
        $file = $this->pathLog.$token.'/results.json';
        $fp = fopen($file, 'w+');
        $info = fread($fp, filesize($file));
        fclose($fp);

        $this->view->render("token", $panel);
    }
}
