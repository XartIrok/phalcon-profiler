<?php

namespace Phalcon;

use Phalcon\Db\Profiler as DbProfiler;
use Phalcon\Db\Adapter as DbAdapter;
use Phalcon\Di;
use Phalcon\Di\InjectionAwareInterface AS Injection;
use Phalcon\DiInterface;
use Phalcon\Loader;
use Phalcon\Mvc\View as View;
use Phalcon\Mvc\Router;
use Phalcon\Mvc\Url;

class Profiler implements Injection
{
    protected $di;
    protected $timeStart;
    protected $timeEnd;
    protected $services = [];
    protected $viewsRendered = [];
    protected $dis = [];
    protected $loggerActive;
    protected $loggerPath;
    protected $dbProfiler;
    protected $queryCount = 0;

    public $tokenNumber;
    public $countViews = 0;

    public function __construct(Di $di, $services = ['db' => ['db'], 'dispatch' => ['dispatcher'], 'view' => ['view']])
    {
        $this->di = $di;
        $this->timeStart = microtime(true);
        $this->dbProfiler = new DbProfiler();

        $eventsManager = $di->get('eventsManager');

        foreach ($di->getServices() as $service) {
            $name = $service->getName();
            foreach ($services as $eventName => $serviceName) {
                if (in_array($name, $serviceName)) {
                    $service->setShared(true);
                    $di->get($name)->setEventsManager($eventsManager);
                    break;
                }
            }
        }
        foreach (array_keys($services) as $eventName) {
            $eventsManager->attach($eventName, $this);
        }
        $this->services = $services;
        $token = $this->generateTokenKey();
        define(DEBUG_TOKEN, $token);
    }

    public function setDI(DiInterface $di)
    {
        $this->di = $di;
    }

    public function getDI()
    {
        return $this->di;
    }

    public function beforeDispatch($event, $dispatcher)
    {
        $controllerClass = $dispatcher->getControllerClass();
        $explode = explode('\\', $controllerClass);
        $class = end($explode).' :: '.$dispatcher->getActiveMethod();

        $this->dis = [
            'namespace' => $dispatcher->getNamespaceName(),
            'action' => $dispatcher->getActiveMethod(),
            'module' => $dispatcher->getModuleName(),
            'namespaceControl' => $controllerClass,
            'controllerAction' => $class,
        ];
    }

    public function getDispatch($name = null)
    {
        return $this->dis[$name];
    }

    public function beforeRenderView($event, View $view, $file)
    {
        $timeRenderStart = microtime(true);
        $params = [];
        $toView = $view->getParamsToView();
        $toView = !$toView ? [] : $toView;

        foreach ($toView as $k => $v) {
            if (is_object($v)) {
                $params[$k] = get_class($v);
            } elseif(is_array($v)) {
                $array = [];

                foreach ($v as $key=>$value) {
                    if (is_object($value)) {
                        $array[$key] = get_class($value);
                    } elseif (is_array($value)) {
                        $array[$key] = 'Array[...]';
                    } else {
                        $array[$key] = $value;
                    }
                }

                $params[$k] = $array;
            } else {
                $params[$k] = (string) $v;
            }
        }

        $path = $view->getActiveRenderPath();
        $key = strstr(strtolower($path), 'views/');
        $this->viewsRendered[$key] = [
            'path' => $path,
            'params' => $params,
            'controller' => $view->getControllerName(),
            'action' => $view->getActionName(),
            'timeRenderStart' => $timeRenderStart,
        ];
    }

    public function afterRenderView($event, View $view, $file)
    {
        $key = strstr(strtolower($view->getActiveRenderPath()), 'views/');
        $this->viewsRendered[$key]['timeRenderEnd'] = microtime(true);
    }

    public function afterRender($event, View $view, $viewFile)
    {
        $this->timeEnd = microtime(true);
        $content = $view->getContent();
        $scripts = '</head>';
        $content = str_replace('</head>', $scripts, $content);
        $rendered = '';
        if (!$this->checkRouterToken())
            $rendered .= $this->renderToolbar();
        $rendered .= '</body>';
        $content = str_replace('</body>', $rendered, $content);

        $view->setContent($content);
    }

    public function getSumRenderTime()
    {
        $sum = 0;
        foreach ($this->viewsRendered as $element) {
            $sum += $element['timeRenderEnd'] - $element['timeRenderStart'];
            $this->countViews++;
        }

        return round($sum, 6);
    }

    protected function checkRouterToken()
    {
        $router = $this->di->get('router');
        return $router->getControllerName() == 'Token' ? true : false;
    }

    protected function renderToolbar()
    {
        $view = new View();
        $viewDir = __DIR__ . '/module/resources/views/';
        $view->setViewsDir($viewDir);
        $view->setVar('profiler', $this);

        $content = $view->getRender('toolbar', 'widget');

        if ($this->loggerActive)
            $this->saveLoggerData();

        return $content;
    }

    public function beforeQuery($event, DbAdapter $connection)
    {
        $this->dbProfiler->startProfile(
            $connection->getRealSQLStatement(),
            $connection->getSqlVariables(),
            $connection->getSQLBindTypes()
        );
    }

    public function afterQuery($event, DbAdapter $connection)
    {
        $this->dbProfiler->stopProfile();
        $this->queryCount++;
    }

    public function getQueryCount()
    {
        return $this->queryCount;
    }

    public function getExhaustedQuery()
    {
        $querys = $this->dbProfiler->getProfiles();
        $time = 0;

        foreach ($querys as $query) {
            $time += $query->getTotalElapsedSeconds();
        }

        return round($time, 2);
    }

    public function getRenderTime()
    {
        return round($this->timeEnd - $this->timeStart, 6);
    }

    public function getResponseCode($short = false)
    {
        $code = http_response_code();

        switch ($code) {
            case 200: $text = 'OK'; break;
            case 404: $text = 'Not Found'; break;
            case 500: $text = 'Internal Server Error'; break;
            default:
                $text = 'Unknown';
        }

        return $short ? $code : $code.' '.$text;
    }

    public function setLoaderRouter(Loader $loader, Router $router)
    {
        $arrLoader = $loader->getNamespaces();
        $arrLoader['Phalcon\Profiler\Controllers'] = __DIR__ . '/module/controllers/';
        $loader->registerNamespaces($arrLoader);
        $loader->register();

        $debug = $router->add('/_profiler/{token}/{panel}', [
            'namespace' => 'Phalcon\Profiler\Controllers',
            'controller' => 'Token',
            'action' => 'index'
        ]);
        $debug->setName('profiler');
    }

    public function setActiveLogger($value = false, $path)
    {
        $this->loggerActive = $value;
        $this->loggerPath = $path;
    }

    protected function saveLoggerData()
    {
        $path = $this->loggerPath.'/logs/'.$this->tokenNumber;
        define(DEBUG_PATH_LOG, $this->loggerPath.'/logs/');
        @mkdir($path, 0777, true);
        $array = array();

        $array['request'] = array(
            'dispatcher' => $this->dis
        );
        $array['query'] = $this->dbProfiler->getProfiles();
        $array['views'] = $this->viewsRendered;
        $array['server'] = array(
            'memory-use' => $this->convert(memory_get_usage(true)),
            'memory-limit' => $this->convert($this->getMemoryLimit()),
            'php' => array(
                'version' => phpversion(),
                'sapi' => php_sapi_name(),
                'phalcon' => \Phalcon\Version::get()
            )
        );

        $fp = fopen($path.'/results.json', 'w+');
        fwrite($fp, json_encode($array, JSON_PRETTY_PRINT));
        fclose($fp);
    }

    protected function generateTokenKey($length = 6)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        $this->tokenNumber = $randomString;
    }

    public function convert($size, $format = null)
    {
        $unit = array('B','KB','MB','GB','TB','PB');
        $value = @round($size/pow(1024,($i=floor(log($size,1024)))),2);
        if ($format == 'value')
            return $value;
        elseif ($format == 'type')
            return $unit[$i];
        else
            return $value.' '.$unit[$i];
    }

    public function getUrl($panel = 'request')
    {
        $url = $this->di->get('url');
        $url->setBaseUri('/');
        return $url->get([
            'for' => '_profiler',
            'token' => $this->tokenNumber,
            'panel' => $panel
        ]);
    }

    public function getMemoryLimit()
    {
        return preg_replace_callback('/(\-?\d+)(.?)/', function ($m) {
            return $m[1] * pow(1024, strpos('BKMG', $m[2]));
        }, strtoupper(ini_get('memory_limit')));
    }
}